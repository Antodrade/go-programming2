package main

import ("fmt"
"runtime"
"sync"
)

func main(){
	fmt.Printf("Número de CPU´s: %v\n",runtime.NumCPU())
	fmt.Printf("Número de Goroutinas: %v\n",runtime.NumGoroutine())

	var wg sync.WaitGroup
	wg.Add(2) 

go func(){
fmt.Println("hola desde la primera goroutina")
wg.Done()
}()

go func(){
	fmt.Println("hola desde la segunda goroutina")
	wg.Done()
}()
fmt.Printf("Número de CPU´s: %v\n",runtime.NumCPU())
	fmt.Printf("Número de Goroutinas: %v\n",runtime.NumGoroutine())

	wg.Wait()

fmt.Println("A punto de finalizar main")
fmt.Printf("Número de CPU´s: %v\n",runtime.NumCPU())
	fmt.Printf("Número de Goroutinas: %v\n",runtime.NumGoroutine())
}